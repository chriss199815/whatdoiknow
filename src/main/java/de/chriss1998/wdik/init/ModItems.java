/*
What Do I know is a Minecraft Forge Mod
Copyright (C) 2019  Christopher Laut

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package de.chriss1998.wdik.init;

import de.chriss1998.wdik.Wdik;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.LinkedHashMap;
import java.util.Map;

public class ModItems {
    public static final Map<String, ItemBlock> BLOCKS_TO_REGISTER = new LinkedHashMap<>();

    public static void  registerAll(RegistryEvent.Register<Item> event){
        if(!event.getName().equals(ForgeRegistries.ITEMS.getRegistryName())) return;
        //Blocks
        BLOCKS_TO_REGISTER.forEach(ModItems::register);

        //Items
        //Nothing yet

    }
    public static<T extends Item> T register(String name, T item){
        ResourceLocation id = Wdik.getID(name);
        item.setRegistryName(id);
        ForgeRegistries.ITEMS.register(item);
        return item;
    }
}
